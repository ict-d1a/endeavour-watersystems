package com.endeavour.helpers;

import com.endeavour.Model;
import jssc.SerialPort;
import jssc.SerialPortException;

import java.nio.charset.StandardCharsets;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * MicroBit functionality handler
 *
 * @author stan albada jelgersma
 */
public class Microbit {
    //variables depending on microbit
    public static boolean microbitIsConnected = false;
    public static boolean sensorsAreInitialized = false;
    public static boolean versionIsInitialized = false;
    public static boolean sensorsAreOperational = false;
    public static boolean softwareIsUpToDate = false;

    public static int amountOfSensorsInSepticTank = -1;
    public static int softwareVersionNumber = -1;
    public static int newestSoftwareVersion = 2;

    //sensor names
    public static final String standardSepticSensorName = "SEPTIC_SENSOR_";
    public static final String standardSepticSensorBottomSuffix = "BOTTOM";
    public static final String standardSepticSensorTopSuffix = "TOP";

    //replies
    public static final String standardMicrobitReply = "REPLY: ";
    public static final String amountOfSensorsReply = "AMOUNT OF SENSORS = ";
    public static final String versionNumberReply = "SOFTWARE VERSION NUMBER = ";

    //requests
    private static final String standardRequest = "REQUEST: ";
    private static final String amountOfSensorsRequest = "AMOUNT OF SENSORS";
    private static final String versionNumberRequest = "SOFTWARE VERSION NUMBER";

    //pump names
    private final String standardPumpName = "_TANK_PUMP";
    private final String drainTankPumpSuffix = "DRAIN";
    private final String fillTankPumpSuffix = "FILL";

    //microbit error messages
    public static final String standardErrorMessage = "ERROR: ";
    public static final String microbitSensorMalfunctionError = "SENSOR_MALFUNCTION";

    //user error messages

    public static final String microbitIsNotConnectedText = "De micro:bit is niet aangesloten!";
    public static final String microbitSensorMalfuntionText = "Er is een niet functionerende sensor!";
    public static final String microbitSoftwareOutdatedText = "De software op uw micro:bit is niet up to date!";

    public enum Message {
        FILL_ONE_LEVEL("FILL_ONE_LEVEL"),
        DRAIN_ONE_LEVEL("DRAIN_ONE_LEVEL");

        private final String name;

        Message(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    /**
     * Initializes new object for microbit
     */
    public Microbit() {
        getAmountOfSensorsInTank();
        //getMicrobitSoftwareVersion();
    }

    public void sendMessage(Message message) {
        //get port from class
        SerialPort serialPort = ComPortSendReceive.serialPort;

        //make string to send to serial port
        String toSend = "";

        if (message == Message.FILL_ONE_LEVEL) {
            toSend = Message.FILL_ONE_LEVEL.getName();
        } else if (message == Message.DRAIN_ONE_LEVEL) {
            toSend = Message.DRAIN_ONE_LEVEL.getName();
        }

        try {
            serialPort.writeString(toSend);
        } catch (SerialPortException e) {
            e.printStackTrace();
        }
    }

    public String[] getDataFromLog(int eventIdRow) {
        //initialize variables
        String query;
        ResultSet result;

        //if value = -1, get highest value in event_id column
        if (eventIdRow == -1) {
            eventIdRow = getHighestValueInEventId();
        }

        //build query
        query = "SELECT * FROM log WHERE event_id=" + eventIdRow;

        //execute statement
        result = executeStatementInLog(query);

        //make returnArray
        String[] returnArray = new String[7];

        //read in results
        try {
            assert result != null;
            if (result.next()) {
                //read results into array
                returnArray[0] = String.valueOf(result.getDate("event_date"));
                returnArray[1] = String.valueOf(result.getTime("event_date"));
                returnArray[2] = result.getString("device_name");
                returnArray[3] = String.valueOf(result.getBoolean("is_output"));
                returnArray[4] = String.valueOf(result.getBoolean("is_digital"));
                returnArray[5] = String.valueOf(result.getInt("sensor_value"));
                returnArray[6] = String.valueOf(result.getInt("event_id"));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        //return
        return returnArray;
    }

    private ResultSet executeStatementInLog(String query) {
        //prepare query
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = Model.getConnection().prepareStatement(query);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        //get result
        ResultSet result = null;
        try {
            assert preparedStatement != null;
            //execute query
            result = preparedStatement.executeQuery(query);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        //return
        return result;
    }

    public int getHighestValueInEventId() {
        //set query
        String query = "SELECT MAX(event_id) FROM log";
        int returnInt = -1;

        //get query result
        ResultSet result = executeStatementInLog(query);

        //get value
        try {
            if (result.next()) {
                try {
                    returnInt = result.getInt(1);
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return returnInt;
    }

    /**
     * method to get current amount of water in septic tank
     *
     * @return septic tank minimum and maximum value in array
     */
    public int[] getCurrentTankFilledPercentage() {
        int sensorNumber = getSepticWaterLevel();

        int stepBetweenLevels = (100 / (amountOfSensorsInSepticTank - 1));

        //calculate values
        int minValue = (stepBetweenLevels * (sensorNumber - 1));
        int maxValue = ((stepBetweenLevels * sensorNumber) - 1);

        //override if values exceed 0 or 100
        if (minValue < 0) {
            minValue = -1;
            maxValue = -1;
        } else if (maxValue > 100) {
            minValue = 100;
            maxValue = 100;
        }

        //make return array
        int[] returnArr = new int[2];

        //fill return array
        returnArr[0] = minValue;
        returnArr[1] = maxValue;

        //return array
        return returnArr;
    }

    public int getSepticWaterLevel() {
        int highestValue = getHighestValueInEventId();
        String[] decode;

        //loop to get last sensor value
        while (true) {
            //get data
            decode = getDataFromLog(highestValue);

            //convert
            boolean isOutput = Boolean.parseBoolean(decode[3]);
            boolean isDigital = Boolean.parseBoolean(decode[4]);

            //check if value is from sensor
            if (((!isOutput) && isDigital)) {
                break;
            }

            //else try row 1 lower
            highestValue--;
        }

        //unpack necessary data
        String deviceName = decode[2];
        deviceName = deviceName.trim();
        int newState = Integer.parseInt(decode[5]);

        //set sensor number
        int sensorNumber = -1;

        //check if bottom sensor
        if (deviceName.equals(standardSepticSensorName + standardSepticSensorBottomSuffix)) {
            sensorNumber = 0;
        }

        //check if top sensor
        if (deviceName.equals(standardSepticSensorName + standardSepticSensorTopSuffix)) {
            sensorNumber = amountOfSensorsInSepticTank - 1;
        }

        //check numeric values
        else {
            for (int i = 1; i < (amountOfSensorsInSepticTank - 1); i++) {
                if (deviceName.equals(standardSepticSensorName + i)) {
                    sensorNumber = i;
                }
            }
        }

        //add 1 if active
        if (newState == 1) {
            sensorNumber++;
        }

        //return
        return sensorNumber;
    }

    /**
     * method to serial print request to microbit
     * request for amount of sensors in septic tank
     */
    public static void getAmountOfSensorsInTank() {
        if (sensorsAreInitialized) return;

        if (ComPortSendReceive.serialPort != null) {
            try {
                String request = standardRequest + amountOfSensorsRequest;
                ComPortSendReceive.serialPort.writeBytes(request.getBytes(StandardCharsets.UTF_8));
            } catch (SerialPortException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * method to serial print request for software version to microbit
     */
    public static void getMicrobitSoftwareVersion() {
        if(versionIsInitialized) return;
        if (ComPortSendReceive.serialPort != null) {
            String request = standardRequest + versionNumberRequest;
            try {
                ComPortSendReceive.serialPort.writeBytes(request.getBytes(StandardCharsets.UTF_8));
            } catch (SerialPortException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * method to get status report of both pumps in septic tank
     * on or off stands for true or false
     * returns at array[0] status of drain tank pump
     * returns at array[1] status of fill tank pump
     */
    public boolean[] getPumpStatusReport() {
        //make returnArray
        boolean[] returnArr = {false, false};

        //get latest data from log
        String[] resultArray = getDataFromLog(-1);

        //check if value is about pump and if pump is active else return
        if (resultArray[2].contains(standardPumpName) && Integer.parseInt(resultArray[5]) == 1) {
            //check if data is about drain pump or fill pump and set necessary values
            if (resultArray[2].contains(drainTankPumpSuffix)) {
                returnArr[0] = true;
            } else if (resultArray[2].contains(fillTankPumpSuffix)) {
                returnArr[1] = true;
            }
        }

        //return values
        return returnArr;
    }

    public static void checkMicrobitSoftwareVersionUpToDate() {
        softwareIsUpToDate = softwareVersionNumber >= newestSoftwareVersion;
    }
}