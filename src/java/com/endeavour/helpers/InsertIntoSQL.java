package com.endeavour.helpers;

import com.endeavour.Model;

import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * @author stan albada jelgersma
 */
public class InsertIntoSQL
{

    /**
     * Insert a new row into the tbl1 table
     *
     */
    public void insertIntoLog(String date, String device_name, boolean is_output, boolean is_digital, int sensor_value) {
        String sql = "INSERT INTO log(event_date, device_name, is_output, is_digital, sensor_value) VALUES(?,?,?,?,?)";
        try {
            PreparedStatement preparedStatement = Model.getConnection().prepareStatement(sql);
            preparedStatement.setTimestamp(1, java.sql.Timestamp.valueOf(date));
            preparedStatement.setString(2, device_name);
            preparedStatement.setBoolean(3, is_output);
            preparedStatement.setBoolean(4, is_digital);
            preparedStatement.setInt(5, sensor_value);
            preparedStatement.executeUpdate();
        }
        catch (Exception e) {
            System.err.println("ongeldige invoer database");
            e.printStackTrace();
        }
    }
}