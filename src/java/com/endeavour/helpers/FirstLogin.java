package com.endeavour.helpers;

import com.endeavour.Main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * @author simon boekestijn
 */
public class FirstLogin {

    private static JFrame frame;

    public static void addComponentsToPane(Container pane) {
        JTextField userField;
        JPasswordField passwordField;
        JButton loginButton;
        JLabel userLabel;
        JLabel passwordLabel;
        pane.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        userLabel = new JLabel("Username:");
        c.gridx = 0;
        c.gridy = 0;
        pane.add(userLabel, c);
        userField = new JTextField("");
        c.gridx = 1;
        c.gridy = 0;
        pane.add(userField, c);
        passwordLabel = new JLabel("Password:");
        c.gridx = 0;
        c.gridy = 1;
        pane.add(passwordLabel, c);
        passwordField = new JPasswordField("");
        c.gridx = 1;
        c.gridy = 1;
        pane.add(passwordField, c);
        loginButton = new JButton("Login");
        c.gridx = 0;
        c.gridy = 2;
        c.gridwidth = 2;
        c.ipadx = 100;
        pane.add(loginButton, c);

        loginButton.addActionListener((ActionEvent e) -> {
            setCredentials(userField.getText(), passwordField.getText());
            frame.setVisible(false);
        });
    }

    public static void createAndShowGUI() {
        frame = new JFrame("LoginPrompt");
        frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        addComponentsToPane(frame.getContentPane());

        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
    }

    public static void setCredentials(String user, String pass) {
        ConfigurationFunctions.writePropValue("user", user);
        ConfigurationFunctions.writePropValue("pass", pass);
        ConfigurationFunctions.writePropValue("firstLogin", "false");
        Main.resourceLoader();
        Main.buildGUI();
    }
}