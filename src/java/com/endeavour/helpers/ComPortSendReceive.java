package com.endeavour.helpers;

import com.endeavour.GUIManager;
import com.endeavour.Main;
import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;
import net.samuelcampos.usbdrivedetector.USBDeviceDetectorManager;

import javax.usb.UsbDevice;
import javax.usb.event.UsbDeviceDataEvent;
import javax.usb.event.UsbDeviceErrorEvent;
import javax.usb.event.UsbDeviceEvent;
import javax.usb.event.UsbDeviceListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Communicate with Microbit
 *
 * @author samuelvisser
 * @author stan albada jelgersma
 */
public class ComPortSendReceive {

    public static SerialPort serialPort;
    private static final List<MicrobitListener> microbitListeners;

    static {
        microbitListeners = new ArrayList<>();
    }

    public static void connectToMicroBit() {

        // Get Micro:Bit from USB
        UsbDevice microBit = DetectUsbDevice.findDevice(HelperFunctions.microBitVendorId, HelperFunctions.microBitProductId);
        serialPort = DetectUsbDevice.getPortFromDevice(microBit);
        boolean deviceFound = !(microBit == null || serialPort == null);

        // Setup detection for devices, in case the Micro:Bit gets disconnected and reconnects again
        USBDeviceDetectorManager driveDetector = new USBDeviceDetectorManager();
        driveDetector.addDriveListener(e -> {
            if (!HelperFunctions.isMicrobitConnected()) {
                UsbDevice newMicroBit = DetectUsbDevice.findDevice(HelperFunctions.microBitVendorId, HelperFunctions.microBitProductId);
                serialPort = DetectUsbDevice.getPortFromDevice(newMicroBit);
                boolean newDeviceFound = !(newMicroBit == null || serialPort == null);
                if (newDeviceFound) loadMicrobit(newMicroBit);
            }
        });

        // Setup connection
        if (deviceFound) {
            loadMicrobit(microBit);
        }
    }

    /**
     * Loads given Microbit into memory
     *
     * @param microBit MicroBit to load
     */
    private static void loadMicrobit(UsbDevice microBit) {
        // Setup serial communication
        boolean connected = openSerialCommunication(microBit);
        HelperFunctions.setMicrobitConnected(connected);

        if(connected) {
            // Add listener for when the MicroBit is disconnected
            microBit.addUsbDeviceListener(new SerialReader.UsbReader());

            // setup microbit for usage
            Main.setMicrobit(new Microbit());
        }
    }

    /**
     * Opens serial communication to the detected serial port
     *
     * @param microBit microBit device to open connection for
     */
    private static boolean openSerialCommunication(UsbDevice microBit) {
        try {
            // seriële poort openen
            serialPort.openPort();
            serialPort.setParams(SerialPort.BAUDRATE_9600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
            serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_RTSCTS_IN | SerialPort.FLOWCONTROL_RTSCTS_OUT);
            serialPort.addEventListener(new SerialReader(), SerialPort.MASK_RXCHAR);
            return true;
        } catch (SerialPortException ex) {
            return false;
        }
    }

    /**
     * Add a listener
     * @param listener
     */
    public static void addMicrobitListener(MicrobitListener listener){
        microbitListeners.add(listener);
    }

    /**
     * Tell all listeners the Microbit is connected
     */
    protected static void fireMicrobitConnected() {
        microbitListeners.forEach(MicrobitListener::onMicrobitConnected);
    }

    /**
     * Tell all listeners the Microbit is disconnected
     */
    protected static void fireMicrobitDisconnected() {
        microbitListeners.forEach(MicrobitListener::onMicrobitDisconnected);
    }

    /**
     * Listens to all data communication coming from the Micro:Bit
     */
    private static class SerialReader implements SerialPortEventListener {
        StringBuilder message = new StringBuilder();
        InsertIntoSQL database = new InsertIntoSQL();

        @Override
        public void serialEvent(SerialPortEvent event) {
            if (event.isRXCHAR() && event.getEventValue() > 0) {
                try {
                    byte[] buffer = serialPort.readBytes();
                    for (byte b : buffer) {
                        if ((b == '\r' || b == '\n') && message.length() > 0) {
                            //convert to string
                            String toProcess = message.toString();

                            System.out.println(toProcess);

                            //set date
                            String dateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

                            //set microbit active
                            Microbit.microbitIsConnected = true;

                            //delete line breaks from date
                            dateTime = dateTime.replace("\n", "").replace("\r", "");

                            //check if message contains reply
                            if (toProcess.contains(Microbit.standardMicrobitReply)) {
                                toProcess = toProcess.replace(Microbit.standardMicrobitReply, "");
                                toProcess = toProcess.trim();

                                //check wich request is replied to
                                if (toProcess.contains(Microbit.amountOfSensorsReply)) {
                                    toProcess = toProcess.replace(Microbit.amountOfSensorsReply, "");
                                    toProcess = toProcess.trim();
                                    Microbit.amountOfSensorsInSepticTank = Integer.parseInt(toProcess);
                                    Microbit.sensorsAreInitialized = true;
                                    Microbit.getMicrobitSoftwareVersion();
                                }
                                if (toProcess.contains(Microbit.versionNumberReply)) {
                                    toProcess = toProcess.replace(Microbit.versionNumberReply, "");
                                    toProcess = toProcess.trim();
                                    Microbit.softwareVersionNumber = Integer.parseInt(toProcess);
                                    Microbit.versionIsInitialized = true;
                                    if(Microbit.softwareVersionNumber >= Microbit.newestSoftwareVersion)
                                    {
                                        Microbit.softwareIsUpToDate = true;
                                    }
                                }

                                //reset values
                                message.setLength(0);

                                //return
                                return;
                            }

                            //check if message contains error message
                            GUIManager gui = Main.getGuiManager();
                            if (toProcess.contains(Microbit.standardErrorMessage)) {
                                toProcess = toProcess.replace(Microbit.standardErrorMessage, "");
                                toProcess = toProcess.trim();

                                if (gui != null) {
                                    if (toProcess.contains(Microbit.microbitSensorMalfunctionError)) {
                                        Microbit.sensorsAreOperational = false;
                                    }
                                }
                            } else {
                                Microbit.sensorsAreOperational = true;
                            }

                            // If message contains values, put in database
                            if (toProcess.contains("%A")) {
                                int messageLength = toProcess.length();
                                int endIndexOfFirstValue = toProcess.indexOf("%A");
                                int endIndexOfSecondValue = toProcess.indexOf("%B");
                                int endIndexOfThirdValue = toProcess.indexOf("%C");

                                String deviceName = toProcess.substring(0, endIndexOfFirstValue);
                                int intIsOutput = Integer.parseInt(toProcess.substring((endIndexOfFirstValue + 2), endIndexOfSecondValue));
                                int intIsDigitalSensor = Integer.parseInt(toProcess.substring((endIndexOfSecondValue + 2), endIndexOfThirdValue));
                                int sensorValue = Integer.parseInt(toProcess.substring((endIndexOfThirdValue + 2), messageLength));

                                //resolve values
                                boolean isOutput = false;
                                boolean isDigitalSensor = false;

                                if (intIsOutput == 1) {
                                    isOutput = true;
                                }
                                if (intIsDigitalSensor == 1) {
                                    isDigitalSensor = true;
                                }

                                //put data in database
                                database.insertIntoLog(dateTime, deviceName, isOutput, isDigitalSensor, sensorValue);

                                //reset values
                                message.setLength(0);
                            }
                            if (Main.getGuiManager() != null) Main.getGuiManager().checkErrorPanel();
                        } else {
                            message.append((char) b);
                        }
                    }
                } catch (SerialPortException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }

        /**
         * Listen to disconnect event from Micro:Bit
         */
        private static class UsbReader implements UsbDeviceListener {
            @Override
            public void usbDeviceDetached(UsbDeviceEvent usbDeviceEvent) {
                HelperFunctions.setMicrobitConnected(false);
            }

            @Override
            public void dataEventOccurred(UsbDeviceDataEvent usbDeviceDataEvent) {
                // This is broken due to very old library
            }

            @Override
            public void errorEventOccurred(UsbDeviceErrorEvent usbDeviceErrorEvent) {
                // This is broken due to very old library
            }
        }
    }
}