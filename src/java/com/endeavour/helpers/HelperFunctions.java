package com.endeavour.helpers;

import com.endeavour.Main;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

/**
 * General helper functions for Endeavour
 *
 * @author stan albada jelgersma
 * @author samuelvisser
 */
public class HelperFunctions {

    /**
     * connection info to database
     */
    public static final String dbName = "endeavour";
    public static final String connectionPath = "jdbc:mysql://localhost:3306/";
    public static final String connectionArgs = "?serverTimezone=UTC";
    public static final String userName = ConfigurationFunctions.readPropValue("user");
    public static final String password = ConfigurationFunctions.readPropValue("pass");

    /**
     * The unique vendorId for the Micro:Bit
     */
    public static final short microBitVendorId = 3368;

    /**
     * The unique productId for the Micro:Bit
     */
    public static final short microBitProductId = 516;

    /**
     * Filepath to fonts for header
     */
    private static final String fontsFilePath = "custom-fonts/static";

    public static void openWebpage(String domainName, int amountOfTimes) {

        // Add https if needed
        String http = "http";
        if(!domainName.startsWith(http)) {
            domainName  = "https://" + domainName;
        }

        // Open links given amount of times
        for (int i = 1; i <= amountOfTimes; i++) {
            URL url = null;
            try {
                url = new URL(domainName);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            try {
                assert url != null;
                openWebpage(url.toURI());
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
    }

    private static void openWebpage(URI uri) {
        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                desktop.browse(uri);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * loads the orbitron font used in the header title
     */
    public static void importOrbitronFont() {
        //construct file path to font
        String filePath = fontsFilePath + "Orbitron-SemiBold.ttf";

        // retieve font url and install it
        URL fontUrl = HelperFunctions.class.getClassLoader().getResource(filePath);
        if (fontUrl != null) {
            try {
                GraphicsEnvironment ge =
                        GraphicsEnvironment.getLocalGraphicsEnvironment();
                ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File(fontUrl.getPath())));
            } catch (FontFormatException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * opens connection with database
     */
    public static void openDatabaseConnection() {

        Connection con = null;
        boolean schemaSet = false;
        try {
            con = DriverManager.getConnection(connectionPath + connectionArgs, userName, password);

            ResultSet resultSet = con.getMetaData().getCatalogs();
            while (resultSet.next()) {
                if(Objects.equals(resultSet.getString(1), dbName)){
                    con.setSchema(dbName);
                    schemaSet = true;
                    break;
                }
            }
            resultSet.close();

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }

        if(con == null || !schemaSet) {
            JOptionPane.showMessageDialog(Main.getMainFrame(),
                    "No local database was found or login information incorrect\nApp will close, you will be prompted to re-login",
                    "Database not found",
                    JOptionPane.ERROR_MESSAGE);
            ConfigurationFunctions.writePropValue("firstLogin", "true");
            System.exit(1);
        }
    }

    public static boolean isMicrobitConnected() {
        return Microbit.microbitIsConnected;
    }

    public static void setMicrobitConnected(boolean isMicrobitConnected) {
        // Set in Microbit class and show error if needed
        Microbit.microbitIsConnected = isMicrobitConnected;
        if(Main.getGuiManager() != null) Main.getGuiManager().checkErrorPanel();

        // Fire connected event
        if(isMicrobitConnected) ComPortSendReceive.fireMicrobitConnected();
        else ComPortSendReceive.fireMicrobitDisconnected();
    }
}