package com.endeavour.helpers;

/**
 * Fires certain Microbit events when they occur
 */
public interface MicrobitListener {
    void onMicrobitConnected();
    void onMicrobitDisconnected();
}
