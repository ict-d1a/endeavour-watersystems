package com.endeavour.helpers;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

/**
 * Class to manage all encryption- related tasks
 *
 * @author samuelvisser
 */
public class Encryptor {

    /**
     * The amount of iterations the encrypter does for each newly created hash
     * The higher this value, the more secure the hash, but the longer it will take
     */
    private static final int iterations = 1000;

    /**
     * Encrypt given string
     *
     * @param input string to encrypt
     * @return encrypted version of the string
     */
    public static String getHash(String input) {
        return getHash(input.toCharArray());
    }

    /**
     * Encrypt given string
     *
     * @param input string to encrypt
     * @return encrypted version of the string
     */
    public static String getHash(char[] input) {
        try {
            byte[] salt = getSalt();

            PBEKeySpec spec = new PBEKeySpec(input, salt, iterations, 64 * 8);
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            byte[] hash = skf.generateSecret(spec).getEncoded();
            return iterations + ":" + toHex(salt) + ":" + toHex(hash);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            System.err.println("Password hashing error: " + e.getMessage());
        }
        return null;
    }

    /**
     * Validates given string against its hash
     *
     * @param toValidateString string to validate
     * @param storedHash hash to validate string against
     * @return whether the string equals the hash
     * @throws NoSuchAlgorithmException if the OS does not recognise the encryption algorithm
     * @throws InvalidKeySpecException if the given hash key is invalid
     */
    public static boolean validateHash(String toValidateString, String storedHash) throws NoSuchAlgorithmException, InvalidKeySpecException {
        return validateHash(toValidateString.toCharArray(), storedHash);
    }

    /**
     * Validates given string against its hash
     *
     * @param toValidate string to validate
     * @param storedHash hash to validate string against
     * @return whether the string equals the hash
     * @throws NoSuchAlgorithmException if the OS does not recognise the encryption algorithm
     * @throws InvalidKeySpecException if the given hash key is invalid
     */
    public static boolean validateHash(char[] toValidate, String storedHash) throws NoSuchAlgorithmException, InvalidKeySpecException {
        String[] parts = storedHash.split(":");
        int iterations = Integer.parseInt(parts[0]);
        byte[] salt = fromHex(parts[1]);
        byte[] hash = fromHex(parts[2]);

        PBEKeySpec spec = new PBEKeySpec(toValidate, salt, iterations, hash.length * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        byte[] testHash = skf.generateSecret(spec).getEncoded();

        int diff = hash.length ^ testHash.length;
        for (int i = 0; i < hash.length && i < testHash.length; i++) {
            diff |= hash[i] ^ testHash[i];
        }
        return diff == 0;
    }

    private static byte[] getSalt() throws NoSuchAlgorithmException {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[16];
        sr.nextBytes(salt);
        return salt;
    }

    private static String toHex(byte[] array) throws NoSuchAlgorithmException {
        BigInteger bi = new BigInteger(1, array);
        String hex = bi.toString(16);
        int paddingLength = (array.length * 2) - hex.length();
        if (paddingLength > 0) {
            return String.format("%0" + paddingLength + "d", 0) + hex;
        } else {
            return hex;
        }
    }

    private static byte[] fromHex(String hex) throws NoSuchAlgorithmException {
        byte[] bytes = new byte[hex.length() / 2];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
        }
        return bytes;
    }
}