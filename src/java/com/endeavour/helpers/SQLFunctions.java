package com.endeavour.helpers;

import com.endeavour.Model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * @author simon boekestijn
 */

public class SQLFunctions {
    static Connection con = Model.getConnection(); // the connection is necessary to interact with the database

    public static void updateSingleValue(String table, String id, String column, String value) {
        try {
            Statement st = con.createStatement(); // UPDATE table SET column = 'value' WHERE ID = 'id';
            String sql = "UPDATE " + table + "\nSET " + column + " = '" + value + "'" + "\nWHERE ID = '" + id + "';";
            st.executeUpdate(sql); // execute the SQL statement to update existing values in the table
        } catch (Exception e) {
            System.err.println("Er is iets misgegaan bij het updaten van de database:");
            e.printStackTrace();
        }
    }

    public static String readSingleValue(String table, String id, String column) {
        try {
            Statement st = con.createStatement(); // SELECT column FROM table WHERE ID = id;
            ResultSet rs = st.executeQuery("SELECT " + column + "\nFROM " + table + "\nWHERE ID = '" + id + "';");
            rs.next(); // make sure we're not grasping at the position *before* the result
            return rs.getString(1); // take the first result from the result set (which should be the only one)
        } catch (Exception e) {
            System.err.println("Er is iets misgegaan bij het updaten van de database of de waarde was nog niet gedefinieerd. Returning 'false'.");
            e.printStackTrace();
            return "false";
        }
    }
}
