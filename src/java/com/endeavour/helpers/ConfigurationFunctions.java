package com.endeavour.helpers;

import java.io.*;
import java.util.Properties;

/**
 * @author simon boekestijn
 */

/*
om te gebruiken; roep de functie als volgt aan, waar property & value je eigen variabelen zijn

ConfigurationFunctions.writePropValue(property,value);
ConfigurationFunctions.readPropValue(property);

 */

public class ConfigurationFunctions {

    private static final String propertiesPath;

    static {
        // all operating systems except for Windows define the path with forward slashes
        // this function adjusts the file path accordingly on computers with a different OS
        propertiesPath = System.getProperty("os.name").startsWith("Win")
                ? "\\endeavourconfig.properties"
                : "/endeavourconfig.properties";
    }

    public static void setupConfigurationFile() {
        File file = new File(System.getProperty("user.home") + propertiesPath); // grab the file path so we can read and write to it
        if (!file.exists()) {
            try { // if it (the configuration file) doesn't already exist, create it; then set up the values we'll need through resetConfig();
                if (file.createNewFile()) {
                    System.out.println("New file created.");
                }
            } catch (IOException e) {
                System.out.println("Something went wrong while creating a new properties file!");
                e.printStackTrace();
            }
            ResetConfig();

        }
        if (readPropValue("subscribedUntil") == null) {
            ResetConfig(); // if the file is outdated (because the property that was last added is null), then reset it
        }

    }

    public static void ResetConfig(){
        writePropValue("firstLogin", "true");
        writePropValue("user","");
        writePropValue("pass", "");
        writePropValue("subscribedUntil", "02 02 2021");
    }

    public static String readPropValue(String property) {
        Properties props = new Properties();
        File file = new File(System.getProperty("user.home") + propertiesPath); // grab the file path so we can read and write to it

        try {
            props.load(new BufferedReader(new FileReader(file)));
        } catch (IOException e) { // if something goes wrong while trying to load the file, throw an error and return null
            System.out.println("Something went wrong while trying to read the properties file!");
            return null;
        }
        return props.getProperty(property); // return the value assigned to a given property imported from the properties file
    }

    public static void writePropValue(String property, String value) {
        Properties props = new Properties();
        File file = new File(System.getProperty("user.home") + propertiesPath); // grab the file path so we can read and write to it

        try (Reader reader = new BufferedReader(new FileReader(file))) {
            props.load(reader);
        } catch (IOException e) { // if something goes wrong while trying to load the file, throw an error
            System.out.println("Something went wrong while trying to read the properties file!");
            e.printStackTrace();
        }

        props.setProperty(property, value); // change the value of the given property to the given value

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) { // write the modified properties back to the file
            Class comment = Class.forName(Thread.currentThread().getStackTrace()[2].getClassName());
            props.store(writer, "last updated by " + comment); // add a comment explaining what class last wrote to the file for debug purposes
        } catch (ClassNotFoundException | IOException e) { // if something goes wrong while trying to write to the file, throw an error
            System.out.println("Something went wrong while writing to the properties file!");
            e.printStackTrace();
        }
    }
}
