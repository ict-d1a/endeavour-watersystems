package com.endeavour.helpers;

import jssc.SerialPort;
import org.jetbrains.annotations.Nullable;
import javax.usb.*;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Detect USB Device
 *
 * @author samuelvisser
 */
public class DetectUsbDevice {

    private static UsbHub rootUsbHub;

    /**
     * Returns the serial port the requested device name is connected to
     *
     * @return serial port if device was found. Null if not
     */
    @Nullable
    public static SerialPort getPortFromDevice(UsbDevice device) {
        if (device == null) return null;

        // Get port based on given OS
        if (System.getProperty("os.name").startsWith("Win")) return getPortForWindows(device);
        else if (System.getProperty("os.name").startsWith("Mac")) return getPortForMacOS(device);
        return null;
    }

    /**
     * Gets SerialPort based on given UsbDevice on MacOS devices
     *
     * @param device usbDevice to get the SerialPort for
     * @return SerialPort for given UsbDevice
     */
    private static SerialPort getPortForMacOS(UsbDevice device) {

        UsbDeviceDescriptor desc = device.getUsbDeviceDescriptor();
        String portName = null;

        try {
            // Get all macOS device data
            Process process = Runtime.getRuntime().exec("ioreg -r -c IOUSBHostDevice -l");
            StreamReader reader = new StreamReader(process.getInputStream());
            reader.start();
            process.waitFor();
            reader.join();
            String output = reader.getResult();

            // Loop over device data to find relevant port data
            String[] result = output.split("\"IOCalloutDevice\" = ");
            if(result.length > 1) {
                for (int i = 0; i < result.length; i++) {
                    if(i % 2 == 0) continue;

                    // Get port name
                    String thisPortName = result[i].trim().substring(1);
                    thisPortName = thisPortName.substring(0, thisPortName.indexOf("\""));

                    // Get port data
                    String data = result[i-1];
                    int dataMiddle = data.lastIndexOf("\"idProduct\"");
                    String thisPortData = data.substring(data.substring(0, dataMiddle).lastIndexOf("{"), data.substring(dataMiddle).indexOf("}") + dataMiddle + 1);

                    // Get product id
                    String prodId = thisPortData.substring(thisPortData.indexOf("\"idProduct\""));
                    prodId = prodId.substring(0, prodId.indexOf("\n")).replaceAll("[^0-9]", "");

                    // Get vendor id
                    String vendorId = thisPortData.substring(thisPortData.indexOf("\"idVendor\""));
                    vendorId = vendorId.substring(0, vendorId.indexOf("\n")).replaceAll("[^0-9]", "");

                    // Check if given port contains device
                    if(desc.idVendor() == Short.parseShort(vendorId) && desc.idProduct() == Short.parseShort(prodId)) {
                        portName = thisPortName;
                        break;
                    }
                }
            }

            if (portName != null) return new SerialPort(portName);
        } catch (Exception e) {
            return null;
        }
        return null;
    }

    /**
     * Gets SerialPort based on given UsbDevice on Windows devices
     *
     * @param device usbDevice to get the SerialPort for
     * @return SerialPort for given UsbDevice
     */
    private static SerialPort getPortForWindows(UsbDevice device) {
        try {
            // Prepare variables
            String regPath = "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Enum\\USB";
            String[] usbDevices = regGetFoldersInPath(regPath);
            UsbDeviceDescriptor desc = device.getUsbDeviceDescriptor();
            String vendorHex = String.format("%04x", desc.idVendor()).toUpperCase();
            String prodHex = String.format("%04x", desc.idProduct()).toUpperCase();
            String devPath = regPath + "\\VID_" + vendorHex + "&PID_" + prodHex;

            // Loop over all devices to find given device, and retract port name
            String portName = null;
            for (String key : usbDevices) {
                if (key.startsWith(devPath)) {
                    String[] deviceFolders = regGetFoldersInPath(key);
                    for (String valDir : deviceFolders) {
                        String[] friendlyName = regGetValueInPath(valDir, "FriendlyName");
                        if (friendlyName.length >= 1 && friendlyName[0].contains("COM")) {
                            Pattern pattern = Pattern.compile("COM[1-9]+");
                            Matcher matcher = pattern.matcher(friendlyName[0]);
                            if (matcher.find()) {
                                portName = matcher.group(0);
                                break;
                            }
                        }
                    }
                    if (portName != null) break;
                }
            }

            if (portName != null) return new SerialPort(portName);
        } catch (IOException | InterruptedException e) {
            return null;
        }
        return null;
    }

    /**
     * Get al list of all folders in given directory
     *
     * @param path directory to look for folders
     * @return list of folders in directory
     * @throws IOException when reading the registry failed for some reason
     * @throws InterruptedException when reading got interrupted
     */
    private static String[] regGetFoldersInPath(String path) throws IOException, InterruptedException {
        return regGetValueInPath(path, null);
    }

    /**
     * Gets value or folders from windows registry path
     *
     * @param path windows path to search in
     * @param value registry value to search for
     * @return requested directory or value
     * @throws IOException when reading the registry failed for some reason
     * @throws InterruptedException when reading got interrupted
     */
    private static String[] regGetValueInPath(String path, @Nullable String value) throws IOException, InterruptedException {
        Process process;
        if (value == null) process = Runtime.getRuntime().exec("reg query \"" + path + "\"");
        else process = Runtime.getRuntime().exec("reg query \"" + path + "\" /v \"" + value + "\"");

        StreamReader reader = new StreamReader(process.getInputStream());
        reader.start();
        process.waitFor();
        reader.join();
        String output = reader.getResult();

        // Parse output
        if (!output.contains("\r\n")) return new String[0];
        if (output.startsWith("\r\n")) output = output.substring(2);
        String[] result = output.split("\r\n");

        // Parse return
        if (value != null && result.length > 1 && Objects.equals(result[0], path)) {
            return new String[]{result[1]};
        }
        return result;
    }

    /**
     * Finds requested devices based on vendor and product id
     *
     * @param vendorId vendor id to look for
     * @param productId product id to look for
     * @return found usb device. Null if not found
     */
    public static UsbDevice findDevice(short vendorId, short productId) {
        if (rootUsbHub == null) {
            try {
                rootUsbHub = UsbHostManager.getUsbServices().getRootUsbHub();
            } catch (UsbException ignored) {
            }
        }
        if (rootUsbHub == null) return null;
        return findDevice(rootUsbHub, vendorId, productId);
    }

    /**
     * Finds requested devices based on hub, vendor and product id
     *
     * @param vendorId vendor id to look for
     * @param productId product id to look for
     * @return found usb device. Null if not found
     */
    public static UsbDevice findDevice(UsbHub hub, short vendorId, short productId) {
        //noinspection unchecked
        for (UsbDevice device : (List<UsbDevice>) hub.getAttachedUsbDevices()) {
            UsbDeviceDescriptor desc = device.getUsbDeviceDescriptor();
            if (desc.idVendor() == vendorId && desc.idProduct() == productId) return device;
            if (device.isUsbHub()) {
                device = findDevice((UsbHub) device, vendorId, productId);
                if (device != null) return device;
            }
        }
        return null;
    }

    /**
     * Reads stream data in separate thread
     * Used for reading Windows registry data
     */
    static class StreamReader extends Thread {
        private final InputStream is;
        private final StringWriter sw = new StringWriter();

        public StreamReader(InputStream is) {
            this.is = is;
        }

        public void run() {
            try {
                int c;
                while ((c = is.read()) != -1)
                    sw.write(c);
            } catch (IOException ignored) {
            }
        }

        public String getResult() {
            return sw.toString();
        }
    }
}
