package com.endeavour;

import com.endeavour.helpers.*;
import com.endeavour.users.UserLogin;
import com.endeavour.users.UserModel;

import javax.swing.*;
import java.awt.*;
import java.util.Objects;

/**
 * Main app
 */
public class Main {

    private static GUIManager guiManager;
    private static JFrame mainFrame;
    private static Microbit microbit;
    private static UserModel userModel;

    /**
     * Start app
     *
     * @param args java startup params
     */
    public static void main(String[] args) {
        // Initialize preferences file
       ConfigurationFunctions.setupConfigurationFile();

        // request login details. This method calls resource loader and buildGUI
        if (Objects.equals(ConfigurationFunctions.readPropValue("firstLogin"), "true")) {
            System.out.println("booting for the first time, giving login prompt");
            FirstLogin.createAndShowGUI();
        } else {
            // load resources
            resourceLoader();

            // login system user
            userModel = new UserModel();
            UserLogin.loginUser();

            // build gui
            buildGUI();
        }
    }

    /**
     * method to load needed resources
     */
    public static void resourceLoader() {
        // open connection with database
        HelperFunctions.openDatabaseConnection();

        // open connection to microbit
        ComPortSendReceive.connectToMicroBit();

        // import orbitron font
        HelperFunctions.importOrbitronFont();
    }

    /**
     * Build application GUI
     */
    public static void buildGUI() {
        //get panel with everything from external class
        guiManager = new GUIManager();
        JPanel mainPanel = guiManager.buildMainPanel();

        //build frame and add main panel
        mainFrame = new JFrame();
        mainFrame.setBounds(300, 300, 600, 500);
        mainFrame.setPreferredSize(new Dimension(1200, 700));
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        mainFrame.add(mainPanel);
        mainFrame.pack();
        mainFrame.setVisible(true);
    }

    public static void refreshView() {
        guiManager.getMainPanel().revalidate();
        guiManager.getMainPanel().repaint();
    }

    public static GUIManager getGuiManager() {
        return guiManager;
    }

    public static JFrame getMainFrame() {
        return mainFrame;
    }

    public static void setMicrobit(Microbit microbit) {
        Main.microbit = microbit;
    }

    public static Microbit getMicrobit() {
        return microbit;
    }

    public static UserModel getUserModel() {
        return userModel;
    }
}