package com.endeavour.leaderboard;

import com.endeavour.Model;
import com.endeavour.View;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Class bound to leaderboard panel form to construct the leaderboard panel
 *
 * @author yassin albogaddaini
 * @author stan albada jelgersma
 */
public class LeaderboardPanelForm implements View {

    private JPanel mainLeaderboardPanel;
    private JTable table1;

    // String query moet veranderd worden, voor ieder apart; geldt ook bij "dbname" in HelperFunctions onder "connection info to database."
    public static final String query = "SELECT * FROM endeavour.leaderbord";

    public LeaderboardPanelForm() {
    }

    /**
     * Return the view's panel
     *
     * @return leaderboard panel
     */
    @Override
    public JPanel getPanel() {
        return mainLeaderboardPanel;
    }

    @Override //runt dit eerst voordat de complete programma opstart
    public void activate() {
        fillTable();

    }

    /**
     * execute query in leaderboard database
     * @param query
     * @return
     */
    public ResultSet executeStatement(String query) {

        //make statement
        PreparedStatement preparedStatement = null;
        try
        {
            preparedStatement = Model.getConnection().prepareStatement(query);
        }
        catch (SQLException throwables)
        {
            throwables.printStackTrace();
        }

        //get result
        ResultSet result = null;
        try
        {
            assert preparedStatement != null;

            //execute query
            result = preparedStatement.executeQuery(query);
        }
        catch (SQLException throwables)
        {
            throwables.printStackTrace();
        }

        return result;
    }

    /**
     * method to fill main table in panel
     */
    public void fillTable()
    {
        //get result via method
        ResultSet result = executeStatement(query);

        //check amount of places in result
        String[][] temp = new String[1000][5];
        int tempcounter = 0;

        //fill array with results
        try {
            while (result.next()) {
                temp[tempcounter][0] = result.getString(1);
                temp[tempcounter][1] = result.getString(2);
                temp[tempcounter][2] = result.getString(3);
                temp[tempcounter][3] = result.getString(4);
                temp[tempcounter][4] = result.getString(5);
                tempcounter++;
            }
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        //make new array
        String[][] newArray = new String[tempcounter][5];

        //fill new array
        for(int i = 0; i < newArray.length; i++)
        {
            newArray[i][0] = temp[i][0];
            newArray[i][1] = temp[i][1];
            newArray[i][2] = temp[i][2];
            newArray[i][3] = temp[i][3];
            newArray[i][4] = temp[i][4];
        }

        //set to table
        table1.setModel(new DefaultTableModel(newArray, new String[]{"Rang", "Gebruiker", "Locatie", "BespaarScore", "ID"}));
    }
}