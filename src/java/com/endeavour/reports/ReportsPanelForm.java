package com.endeavour.reports;

import com.endeavour.Main;
import com.endeavour.View;
import com.endeavour.helpers.ComPortSendReceive;
import com.endeavour.helpers.Microbit;
import com.endeavour.helpers.MicrobitListener;
import com.endeavour.home.HomePanelForm;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;

import javax.swing.*;
import java.awt.*;

/**
 * Class bound to reports panel form to construct the reports panel
 *
 * @author johan edward
 * @author stan albada jelgesma
 * @author samuelvisser
 *
 * @see <a href="https://www.tutorialspoint.com/how-to-change-jframe-background-color-in-java">iFrame bg color</a>
 * @see <a href="https://docs.oracle.com/javase/tutorial/uiswing/components/progress.html">Progress Bars</a>
 */
public class ReportsPanelForm implements View, MicrobitListener {
    private JPanel mainReportsPanel;
    private JButton maakDeBuizenSchoonButton;
    private JProgressBar waterflowProgressBar;
    private JProgressBar pump1ProgressBar;
    private JButton fillTankButton;
    private JProgressBar pump2ProgressBar;
    private JButton drainTankButton;
    private JButton refreshButton;
    private JLabel statusRapportenText;
    private JLabel tankFilledTextField;
    private JProgressBar tankValueProgressBar;
    private JPanel tankValuePanel;
    private JLabel statusPompenText;
    private JLabel inhoudSepticTankText;
    private JLabel waterflowText;
    private JPanel chartPanel;
    private JLabel comboBoxTitle;
    private JComboBox dataComboBox;

    // Array's initialiseren
    JPanel[] inhoudTank;
    JPanel[] sensorPanels;

    //object maken voor microbit
    Microbit microbit = new Microbit();

    //andere variabelen
    String standardSepticSensorName = "Sensor ";

    public ReportsPanelForm() {
        //add action listeners
        maakDeBuizenSchoonButton.addActionListener(e -> {
            JOptionPane.showMessageDialog(null, "De buizen worden nu schoon gemaakt!");
            microbit.sendMessage(Microbit.Message.DRAIN_ONE_LEVEL);
        });
        fillTankButton.addActionListener(e -> microbit.sendMessage(Microbit.Message.FILL_ONE_LEVEL));
        drainTankButton.addActionListener(e -> microbit.sendMessage(Microbit.Message.DRAIN_ONE_LEVEL));
        refreshButton.addActionListener(e -> activate());

        //add item listener to combo box
        dataComboBox.addActionListener(e -> activate());

        // Register as listener (to get connected and disconnected messages)
        ComPortSendReceive.addMicrobitListener(this);
    }

    @Override
    public void onMicrobitConnected() {
        loadMicrobitData();
    }

    @Override
    public void onMicrobitDisconnected() {
        loadMicrobitData();
    }

    public void makeArrays() {
        if (!Microbit.sensorsAreOperational || ComPortSendReceive.serialPort == null) {
            return;
        }

        if(!(Microbit.amountOfSensorsInSepticTank < 1)) {
            inhoudTank = new JPanel[Microbit.amountOfSensorsInSepticTank - 1];
            sensorPanels = new JPanel[Microbit.amountOfSensorsInSepticTank - 1];
        }
    }

    public void makeTankFilledProgressBar() {
        //set max and min
        tankValueProgressBar.setMinimum(0);
        tankValueProgressBar.setMaximum(100);

        //set standard colors
        tankValueProgressBar.setForeground(Color.cyan);
        tankValueProgressBar.setBackground(Color.white);

        //set tank as inactive if microbit is not connected
        if (ComPortSendReceive.serialPort == null || (!Microbit.microbitIsConnected)) {
            tankValueProgressBar.setBackground(Color.lightGray);
            tankValueProgressBar.setValue(0);
        }

        //disable tank value progress bar if sensors are not operational
        if (!Microbit.sensorsAreOperational && Microbit.sensorsAreInitialized) {
            tankValueProgressBar.setBackground(Color.red);
            tankValueProgressBar.setValue(0);
        }
    }

    public void setTankFilledProgressBarValue() {
        if (!Microbit.sensorsAreOperational || !Microbit.microbitIsConnected) {
            return;
        }

        //make variables
        Microbit microbit = new Microbit();
        int[] values = microbit.getCurrentTankFilledPercentage();
        int averageValue = (values[0] + values[1]) / 2;
        tankValueProgressBar.setValue(averageValue);
    }

    public void setTankFilledTextField() {
        HomePanelForm homePanel = new HomePanelForm();
        String text = homePanel.getTextForFilledValue();
        tankFilledTextField.setText(text);
    }

    public void disableButtonsIfSystemIsNotActive() {
        boolean enabled = true;
        if (ComPortSendReceive.serialPort == null) {
            enabled = false;
        }
        fillTankButton.setEnabled(enabled);
        drainTankButton.setEnabled(enabled);
        maakDeBuizenSchoonButton.setEnabled(false);
    }

    /**
     * Return the view's panel
     *
     * @return reports panel
     */
    @Override
    public JPanel getPanel() {
        return mainReportsPanel;
    }

    @Override
    public void activate() {
        // Reloads Microbit data each time the form is opened
        loadMicrobitData();

        //nog iets anders
        refreshButton.setFocusPainted(false);
        fillTankButton.setFocusPainted(false);
        drainTankButton.setFocusPainted(false);

        //make chart and append to panel
        addChartToPanel();

    }

    /**
     * Loads in Microbit data
     */
    private void loadMicrobitData() {
        Microbit.getMicrobitSoftwareVersion();
        disableButtonsIfSystemIsNotActive();
        makeArrays();
        makeTankFilledProgressBar();
        setTankFilledProgressBarValue();
        setTankFilledTextField();
    }

    /**
     * method to read combo box and get chart
     * @return chart with tank levels
     */
    private JFreeChart getChart()
    {
        //make date
        databaseChart.date dateToGet = databaseChart.date.LAST_24_HOURS;

        //determine date
        if(databaseChart.date.LAST_WEEK.getName().equals(dataComboBox.getSelectedItem())) {
            dateToGet = databaseChart.date.LAST_WEEK;
        }
        else if(databaseChart.date.LAST_MONTH.getName().equals(dataComboBox.getSelectedItem())) {
            dateToGet = databaseChart.date.LAST_MONTH;
        }

        //return chart
        return databaseChart.getChart(dateToGet);
    }

    private void addChartToPanel() {
        chartPanel.removeAll();
        JFreeChart chart = getChart();
        ChartPanel CP = new ChartPanel(chart);
        chartPanel.add(CP);
        Main.refreshView();
    }
}