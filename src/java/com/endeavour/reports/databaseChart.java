package com.endeavour.reports;
import com.endeavour.Model;
import com.endeavour.helpers.Microbit;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.DefaultCategoryDataset;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.sql.*;
import java.util.Date;

/**
 * class to get tank level chart and other operations
 * @author Stan Albada Jelgersma
 */

public class databaseChart
{
    enum date
    {
        LAST_24_HOURS("24 uur"),
        LAST_WEEK("Week"),
        LAST_MONTH("Maand");

        private final String name;

        date(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
    public static JFreeChart getChart(date date)
    {
        DefaultCategoryDataset dataset = getDataSetFromDatabase(date);
        return ChartFactory.createLineChart("Tank inhoud", "tijd", "Tank procent", dataset);
    }
    private static DefaultCategoryDataset getDataSetFromDatabase(date date)
    {
        //get current date and time
        LocalDateTime currDateTime = LocalDateTime.now();

        //determine date to get
        LocalDateTime dateTimeToGet = currDateTime.minusDays(1);
        if(date == databaseChart.date.LAST_WEEK)
        {
            dateTimeToGet = currDateTime.minusWeeks(1);
        }
        else if(date == databaseChart.date.LAST_MONTH)
        {
            dateTimeToGet = currDateTime.minusMonths(1);
        }

        //construct sql statement
        String sqlQuery = "select event_date, device_name, sensor_value from log where is_output = 0 and is_digital = 1 and event_date >= '" + dateTimeToGet + "';";

        //execute sql statement and get result
        ResultSet toProcess = executeQueryOnDatabase(sqlQuery);
        
        //clone resultset
        ResultSet toProcessClone = executeQueryOnDatabase(sqlQuery);

        //make dataset
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        //other vars
        int amountOfSensors = 2;
        int highestNumberFound = 0;

        //first find highest sensor value
        try
        {
            //first find highest amount of sensors
            while(toProcess.next())
            {
                String deviceName = toProcess.getString(2);
                char lastLetter = deviceName.charAt(deviceName.length() - 1);
                if(lastLetter == '0' || lastLetter == '1' || lastLetter == '2' || lastLetter == '3' || lastLetter == '4' || lastLetter == '5' || lastLetter == '6' || lastLetter == '7' || lastLetter == '8' || lastLetter == '9')
                {
                    //to int
                    int numberFound = Character.getNumericValue(lastLetter);
                    if(numberFound > highestNumberFound)
                    {
                        highestNumberFound = numberFound;
                    }
                }
            }

            //calculate amount of sensors
            amountOfSensors += highestNumberFound;

            //vars
            int stepBetweenLevels = (100 / amountOfSensors);

            //loop to add values to dataset
            while(toProcessClone.next())
            {
                String deviceName = toProcessClone.getString(2).trim();
                int sensorNumber = -1;

                //check if top or bottom
                if(deviceName.equals(Microbit.standardSepticSensorName + Microbit.standardSepticSensorTopSuffix))
                {
                    sensorNumber = amountOfSensors - 1;
                }
                else if(deviceName.equals(Microbit.standardSepticSensorName + Microbit.standardSepticSensorBottomSuffix))
                {
                    sensorNumber = 0;
                }

                //base number on sensor number in result
                else
                {
                    sensorNumber = deviceName.charAt(deviceName.length() - 1);
                }

                //calculate
                int sensorValue = toProcessClone.getInt(3);
                int tankLevelPercent = stepBetweenLevels * (sensorNumber + sensorValue);

                //get date
                Date eventDate = toProcessClone.getDate(1);
                Time eventTime = toProcessClone.getTime(1);

                //set correct date format
                String stringEventDateTime = "empty";
                if(date == databaseChart.date.LAST_24_HOURS)
                {
                    DateFormat dateFormat = new SimpleDateFormat("kk:mm");
                    stringEventDateTime = dateFormat.format(eventTime);
                }
                else if(date == databaseChart.date.LAST_WEEK)
                {
                    String tempEventDate = new SimpleDateFormat("dd").format(eventDate);
                    String tempEventTime = new SimpleDateFormat("kk:mm").format(eventTime);
                    stringEventDateTime = tempEventDate + " / " + tempEventTime;

                }
                else if(date == databaseChart.date.LAST_MONTH)
                {
                    String tempEventDate = new SimpleDateFormat("dd").format(eventDate);
                    String tempEventTime = new SimpleDateFormat("kk").format(eventTime);
                    stringEventDateTime = tempEventDate + " / " + tempEventTime;
                }

                //add to dataset
                dataset.addValue(tankLevelPercent, "Water level", stringEventDateTime);
            }
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }

        //return
        return dataset;
    }
    private static ResultSet executeQueryOnDatabase(String query)
    {
        //make statement
        PreparedStatement preparedStatement = null;
        try
        {
            preparedStatement = Model.getConnection().prepareStatement(query);
        }
        catch (SQLException throwables)
        {
            throwables.printStackTrace();
        }

        //get result
        ResultSet result = null;
        try
        {
            assert preparedStatement != null;

            //execute query
            result = preparedStatement.executeQuery(query);
        }
        catch (SQLException throwables)
        {
            throwables.printStackTrace();
        }

        return result;
    }
}
