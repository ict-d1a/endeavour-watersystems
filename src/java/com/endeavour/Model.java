package com.endeavour;

import com.endeavour.helpers.HelperFunctions;

import java.sql.Connection;
import java.sql.DriverManager;

public class Model {

    private static Connection connection;

    /**
     * Connect to the vb1.db database
     *
     * @return the Connection object
     */
    public static Connection getConnection() {
        if(connection == null) {
            String driver = "com.mysql.cj.jdbc.Driver";
            // MySQL connection string, pas zonodig het pad aan:
            String conString = HelperFunctions.connectionPath + HelperFunctions.dbName + HelperFunctions.connectionArgs;
            String user = HelperFunctions.userName;
            String password = HelperFunctions.password;
            try {
                Class.forName(driver);
                connection = DriverManager.getConnection(conString, user, password);
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }
        return connection;
    }
}
