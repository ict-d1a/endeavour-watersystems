package com.endeavour.care;

import com.endeavour.Model;
import com.endeavour.View;
import com.endeavour.helpers.ConfigurationFunctions;
import com.endeavour.helpers.HelperFunctions;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Class bound to care panel form to construct the care panel
 *
 * @author simon boekestijn
 * @author milan groot
 * @author Stan Albada Jelgersma
 */
public class CarePanelForm implements View {

    private JPanel mainCarePanel;
    private JButton wijn;
    private JButton bier;
    private JLabel SubTimer;
    private JLabel Loop;
    private JButton schrijfEenReviewButton;
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    String[] reviews;
    int currentReview = 0;

    // initializing button
    public CarePanelForm() {
        addActionlistenersToButtons();
        updateSubscriptionField();
        scrollThroughReviews();
    }

    /**
     * Return the view's panel
     *
     * @return care panel
     */

    @Override
    public JPanel getPanel() {
        return mainCarePanel;
    }

    @Override
    public void activate() {
        writeResultsToArray();
    }

    public void scrollThroughReviews() {
        final Runnable changereview = new Runnable() {
            public void run() {
                nextReview();
            }
        };
        final ScheduledFuture<?> changereviewHandle =
                scheduler.scheduleAtFixedRate(changereview, 3, 3, SECONDS);
    }

    public void nextReview () {
        if (currentReview == reviews.length) {
            currentReview = 0;
        }
        Loop.setText(reviews[currentReview]);
        currentReview++;
    }

    private void addActionlistenersToButtons() {
        bier.addActionListener(e -> HelperFunctions.openWebpage("www.19047312.wixsite.com/website", 1));
        wijn.addActionListener(e -> HelperFunctions.openWebpage("www.19047312.wixsite.com/", 1));
        writeReviewForm writereviewform = new writeReviewForm();
        schrijfEenReviewButton.addActionListener(e -> writereviewform.makeReviewFrame());
        bier.setFocusPainted(false);
        wijn.setFocusPainted(false);
        schrijfEenReviewButton.setFocusPainted(false);
    }

    public void updateSubscriptionField() {
        LocalDate lastDayofCurrentMonth = LocalDate.now().with(TemporalAdjusters.lastDayOfMonth());
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd MM yyyy");
        String subscriptionEndDate = ConfigurationFunctions.readPropValue("subscribedUntil");
        LocalDate CarePlusEnd = LocalDate.parse(subscriptionEndDate, dtf);
        LocalDate currentDate = LocalDate.now();
        long daysBetween = ChronoUnit.DAYS.between(currentDate, CarePlusEnd);
        long DaysUntilBill = ChronoUnit.DAYS.between(currentDate, getLastWorkingDayOfMonth(lastDayofCurrentMonth));
        SubTimer.setText("Je bent nog een Care+ abonnementshouder tot " + subscriptionEndDate + " || Dagen tot je subscriptie over is: " + daysBetween + " || Je volgende factuur ontvang je over " + DaysUntilBill + " dagen.");
    }

    public static LocalDate getLastWorkingDayOfMonth(LocalDate lastDayOfMonth) {
        LocalDate.now().with(TemporalAdjusters.lastDayOfMonth());
        return switch (DayOfWeek.of(lastDayOfMonth.get(ChronoField.DAY_OF_WEEK))) {
            case SATURDAY -> lastDayOfMonth.minusDays(1);
            case SUNDAY -> lastDayOfMonth.minusDays(2);
            default -> lastDayOfMonth;
        };
    }

    /**
     * leest reviews uit vanuit de database en print ze uit op de review plek met een timer
     */
    public void writeResultsToArray() {
        String firstquery = "SELECT COUNT(*) FROM endeavour.user_review";
        ResultSet firstresult = executeStatement(firstquery);
        int amountOfResults = -1;
        try {
            firstresult.next();
            amountOfResults = firstresult.getInt(1);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        reviews = new String[amountOfResults];

        String secondquery = "SELECT * FROM endeavour.user_review";
        ResultSet secondresult = executeStatement(secondquery);
            try {
                int counter = 0;
                while (secondresult.next()) {
                    reviews[counter] = secondresult.getString(2);
                    counter++;
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
    }

    public ResultSet executeStatement(String query) {

        //make statement
        PreparedStatement preparedStatement = null;
        try
        {
            preparedStatement = Model.getConnection().prepareStatement(query);
        }
        catch (SQLException throwables)
        {
            throwables.printStackTrace();
        }

        //get result
        ResultSet result = null;
        try
        {
            assert preparedStatement != null;

            //execute query
            result = preparedStatement.executeQuery(query);
        }
        catch (SQLException throwables)
        {
            throwables.printStackTrace();
        }

        return result;
    }
}