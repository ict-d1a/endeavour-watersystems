package com.endeavour.care;
import com.endeavour.Model;
import com.endeavour.View;
import javax.swing.*;
import java.awt.event.WindowEvent;
import java.sql.PreparedStatement;

/**
 * class to write a review with GUI
 * @author Stan Albada Jelgersma
 */

public class writeReviewForm implements View {
    private JPanel mainPanel;
    private JLabel mainTitle;
    private JButton bevestigButton;
    private JButton annuleerButton;
    private JPanel mainContentPanel;
    private JTextPane mainTextPane;
    private JFrame mainFrame;
    private String mainTextPaneTitle = "Schrijf hier uw review";

    public void makeReviewFrame()
    {
        mainFrame = new JFrame();
        addAllActionListenersToButtons();
        setAllButtonsFocusPainted(false);
        mainFrame.add(mainPanel);
        mainFrame.setBounds(500, 500, 500, 350);
        mainFrame.setVisible(true);
        mainFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }
    public writeReviewForm()
    {

    }
    private void setAllButtonsFocusPainted(boolean value)
    {
        annuleerButton.setFocusPainted(value);
        bevestigButton.setFocusPainted(value);
    }
    private void addAllActionListenersToButtons()
    {
        annuleerButton.addActionListener(e -> mainFrame.dispatchEvent(new WindowEvent(mainFrame, WindowEvent.WINDOW_CLOSING)));
        bevestigButton.addActionListener(e -> exportReviewToDatabase());
        bevestigButton.addActionListener(f -> mainFrame.dispatchEvent(new WindowEvent(mainFrame, WindowEvent.WINDOW_CLOSING)));

    }
    private void exportReviewToDatabase()
    {
        String toWrite = mainTextPane.getText();
        toWrite = toWrite.trim();
        String sqlQuery = "insert into user_review (review) values ('" + toWrite + "');";

        //exit if no value is entered
        if(toWrite.equals(mainTextPaneTitle) || toWrite.equals(""))
        {
            return;
        }
        try {
            PreparedStatement preparedStatement = Model.getConnection().prepareStatement(sqlQuery);
            preparedStatement.executeUpdate();
        }
        catch (Exception e) {
            System.err.println("ongeldige invoer database");
            e.printStackTrace();
        }
    }

    @Override
    public JPanel getPanel() {
        return null;
    }

    @Override
    public void activate() {

    }
}
