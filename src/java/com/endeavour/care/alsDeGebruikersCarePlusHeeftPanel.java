package com.endeavour.care;

import javax.swing.*;

/**
 * @author johan edward
 * @see <a href="https://stackoverflow.com/questions/16919727/javafx-how-to-disable-a-button-for-a-specific-amount-of-time#16920187">Stack Overflow</a>
 */
public class alsDeGebruikersCarePlusHeeftPanel {
    private JPanel ExclusieveCarePlus;
    private JLabel HulpTekst;
    private JButton drukHierOpOmButton;
    boolean heeftGebruikerCarePlusNodig = false;

    public alsDeGebruikersCarePlusHeeftPanel() {

        drukHierOpOmButton.addActionListener(e -> {
            if(heeftGebruikerCarePlusNodig){
                JOptionPane.showMessageDialog(null, "We komen met je in contact!");
                heeftGebruikerCarePlusNodig = true;
            }
            else{
                JOptionPane.showMessageDialog(null, "We komen nu niet met je in contact");
                heeftGebruikerCarePlusNodig = false;
                try {
                    Thread.sleep(10800000); // Zorgt ervoor dat je het niet kan spam klikken! Knop blijft 3 uur niet beschrikbaar!
                }
                catch (InterruptedException interruptedException) {
                    interruptedException.printStackTrace();
                }

            }
        });
    }

    private void createUIComponents() {
        HulpTekst.setText("Als er iets mis is met je watersysteem, dan kan je op onderstaande knop drukken zodat onze klantenservice team met je in contact kan komen. \n Bedank in je vertrouwen in ons!");
    }

    public JPanel getPanel() {
        return ExclusieveCarePlus;
    }
}