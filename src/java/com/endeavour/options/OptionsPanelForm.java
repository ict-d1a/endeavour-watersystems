package com.endeavour.options;

import com.endeavour.View;
import com.endeavour.helpers.ConfigurationFunctions;
import com.endeavour.helpers.SQLFunctions;

import javax.swing.*;

/**
 * Class bound to options panel form to construct the options panel
 *
 * @author simon boekestijn
 */

public class OptionsPanelForm implements View {
    private JPanel mainOptionsPanel;
    private JRadioButton darkmodeToggle;
    private JRadioButton dataSharingToggle;
    private JRadioButton participationToggle;
    private JButton resetButton;
    private JTextField usernameInput;
    private JLabel currentUser;

    // initialize
    public OptionsPanelForm() {
        // fetch the current nickname and settings so they display properly
        currentUser.setText(SQLFunctions.readSingleValue("users", "1", "nickname"));
        if (SQLFunctions.readSingleValue("users", "1", "darkmode").equals("true")) {
            darkmodeToggle.setSelected(true);
        }
        if (SQLFunctions.readSingleValue("users", "1", "datasharing").equals("true")) {
            dataSharingToggle.setSelected(true);
        }
        if (SQLFunctions.readSingleValue("users", "1", "participation").equals("true")) {
            participationToggle.setSelected(true);
        }
        // add action listeners to buttons so that functions are called when they're clicked
        darkmodeToggle.addActionListener(e -> darkModeToggle());
        dataSharingToggle.addActionListener(e -> dataSharingToggle());
        resetButton.addActionListener(e -> resetAccount());
        participationToggle.addActionListener(e -> participationToggle());
        usernameInput.addActionListener(e -> setNickname(usernameInput.getText()));

    }

    /**
     * Return the view's panel
     *
     * @return options panel
     */
    @Override
    public JPanel getPanel() {
        return mainOptionsPanel;
    }

    @Override
    public void activate() {

    }
    // read the current value of a given setting so we know whether to set it to true or set it to false;
    // then display a message prompt for the user while updating the value in their database record;
    // do this three times, with slightly different messages, for the three radio buttons
    public void participationToggle() {
        if (SQLFunctions.readSingleValue("users", "1", "participation").equals("false")) {
            JOptionPane.showMessageDialog(null, "Je doet nu mee aan de competitie onder de naam " + SQLFunctions.readSingleValue("users", "1", "nickname") + ".");
            SQLFunctions.updateSingleValue("users", "1", "participation", "true");
        } else {
            JOptionPane.showMessageDialog(null, "Je doet nu niet meer mee aan de competitie.");
            SQLFunctions.updateSingleValue("users", "1", "participation", "false");
        }
    }

    public void dataSharingToggle() {
        if (SQLFunctions.readSingleValue("users", "1", "datasharing").equals("false")) {
            JOptionPane.showMessageDialog(null, "Je deelt nu geanonimiseerde data met ons.");
            SQLFunctions.updateSingleValue("users", "1", "datasharing", "true");
        } else {
            JOptionPane.showMessageDialog(null, "Je deelt niet langer geanonimiseerde data met ons.");
            SQLFunctions.updateSingleValue("users", "1", "datasharing", "false");
        }
    }

    public void darkModeToggle() {
        if (SQLFunctions.readSingleValue("users", "1", "darkmode").equals("false")) {
            JOptionPane.showMessageDialog(null, "Dark Mode staat nu aan.");
            SQLFunctions.updateSingleValue("users", "1", "darkmode", "true");
        } else {
            JOptionPane.showMessageDialog(null, "Dark Mode staat nu uit.");
            SQLFunctions.updateSingleValue("users", "1", "darkmode", "false");
        }
    }
    // configurationfunctions only prompts for login information on first boot, which it keeps track of through a value in the properties file;
    // we set this value to true again and then shut down the application for it to be rebooted (prompting the user to login again)
    public void resetAccount() {
        JOptionPane.showMessageDialog(null, "Je bent succesvol uitgelogd. De app wordt nu afgesloten zodat je opnieuw kan inloggen.");
        ConfigurationFunctions.writePropValue("firstLogin", "true");
        System.exit(0);
    }

    // grabs the text input by the user, changes the one on display in the little information block to the right,
    // clears the text input box, then updates the value in the database and notifies the user
    public void setNickname(String newnick) {
        currentUser.setText(newnick);
        usernameInput.setText("");
        SQLFunctions.updateSingleValue("users", "1", "nickname", newnick);
        JOptionPane.showMessageDialog(null, "Je nickname voor de competitie is aangepast.");
    }
}