package com.endeavour.home;

import com.endeavour.Main;
import com.endeavour.View;
import com.endeavour.helpers.ComPortSendReceive;
import com.endeavour.helpers.Microbit;

import javax.swing.*;

/**
 * Class bound to home panel form to construct the home panel
 */
public class HomePanelForm implements View {

    private JPanel mainHomePanel;
    private JList list1;
    private JLabel filledValue;
    private JPanel valuePanel;

    private final Microbit microbit;

    public HomePanelForm() {
        this.microbit = Main.getMicrobit();
    }

    /**
     * Return the view's panel
     *
     * @return home panel
     */
    @Override
    public JPanel getPanel() {
        return mainHomePanel;
    }

    @Override
    public void activate() {
        Main.getGuiManager().checkErrorPanel();
        filledValue.setText(getTextForFilledValue());
    }

    private void createUIComponents() {

    }

    public String getTextForFilledValue() {
        String text;

        //define text
        String standardText = "De tank is tussen de %d en %d procent gevuld";
        String emptyText = "De tank is leeg";
        String fullText = "De tank is vol";

        try {
            int[] valuesArray = microbit.getCurrentTankFilledPercentage();
            //check if tank is empty
            if ((valuesArray[0] <= 0) && (valuesArray[1] <= 0)) {
                text = emptyText;
            }

            //check if tank is full
            else if ((valuesArray[0] >= 100) && (valuesArray[1] >= 100)) {
                text = fullText;
            }

            //else set retrieved values
            else {
                text = String.format(standardText, valuesArray[0], valuesArray[1]);
            }

            //check if system is not active
            if (ComPortSendReceive.serialPort == null || !Microbit.microbitIsConnected) {
                text = Microbit.microbitIsNotConnectedText;
            }

            //check for errors
            if((!Microbit.sensorsAreOperational) && Microbit.sensorsAreInitialized)
            {
                text = Microbit.microbitSensorMalfuntionText;
            }
        } catch (NullPointerException e) {
            text = Microbit.microbitIsNotConnectedText;
        }

        //return
        return text;
    }
}