package com.endeavour;

import com.endeavour.care.CarePanelForm;
import com.endeavour.helpers.HelperFunctions;
import com.endeavour.helpers.Microbit;
import com.endeavour.home.HomePanelForm;
import com.endeavour.leaderboard.LeaderboardPanelForm;
import com.endeavour.options.OptionsPanelForm;
import com.endeavour.reports.ReportsPanelForm;
import com.endeavour.users.UserPanelForm;
import org.jetbrains.annotations.NotNull;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.net.URL;
import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.Map;

/**
 * Main GUI manager
 * Loads in panels and changes app title
 *
 * @author stan albada jelgersma
 * @author samuelvisser
 */
public class GUIManager {
    // All GUI components
    private JPanel mainPanel;
    private JPanel menuPanel;
    private JPanel headerPanel;
    private JButton homeButton;
    private JButton reportsButton;
    private JButton leaderboardButton;
    private JButton careButton;
    private JButton optionsButton;
    private JButton helpButton;
    private JLabel headerLabelTitle;
    private JPanel contentPanel;
    private JLabel errorPanel;
    private JButton usersButton;

    // Btn names
    private final String homeBtnName;
    private final String reportsBtnName;
    private final String leaderboardBtnName;
    private final String careBtnName;
    private final String optionsBtnName;
    private final String usersBtnName;
    private final String helpBtnName;

    private final Map<String, View> createdViews;

    /**
     * filepath to icons
     */
    private final static String iconsFilePath = "menu-icons/";

    /**
     * Filepath to fonts
     */
    public final static String fontsFilePath = "custom-fonts/";

    /**
     * Image format
     */
    private final static String imgFileType = ".PNG";

    //set size of icons in menuPanel
    //the source images have a ratio of 64 width to 53 height
    int iconWidth = 64;
    int iconHeight = 53;

    //set current active button
    private JButton currentActiveButton;

    //set default header title
    private final String defaultHeaderTitle = "WATERLOOP-INATOR 3000";

    /**
     * List of all states the buttons can be in
     */
    enum BtnState {
        DEFAULT,
        HOVER,
        ACTIVE
    }

    /**
     * Initializes GUI manager
     */
    public GUIManager() {
        homeBtnName = homeButton.getName();
        reportsBtnName = reportsButton.getName();
        leaderboardBtnName = leaderboardButton.getName();
        careBtnName = careButton.getName();
        optionsBtnName = optionsButton.getName();
        usersBtnName = usersButton.getName();
        helpBtnName = helpButton.getName();

        currentActiveButton = homeButton;
        createdViews = new HashMap<>();
    }

    public JPanel getMainPanel (){
        return mainPanel;
    }

    /**
     * Always runs when the microbit connection status is set anywhere
     * Hides or shows Micro:Bit message on top
     */
    public void toggleErrorPanel(String text, boolean changeToVisible) {
        if (!HelperFunctions.isMicrobitConnected()) text = "Micro:Bit is niet aangesloten!";
        errorPanel.setText(text);
        errorPanel.setVisible(changeToVisible);
    }

    /**
     * checks if error panel needs activatoin or not
     */
    public void checkErrorPanel() {
        String[] text = new String[10];
        String textInBetweenMessages = " & ";
        boolean noErrors = true;

        if (!Microbit.microbitIsConnected) {
            //immediately set text and return
            toggleErrorPanel(Microbit.microbitIsNotConnectedText, true);
            return;
        }

        if (!Microbit.softwareIsUpToDate && Microbit.versionIsInitialized) {
            text[0] = Microbit.microbitSoftwareOutdatedText;
        }

        if (!Microbit.sensorsAreOperational && Microbit.sensorsAreInitialized) {
            text[1] = Microbit.microbitSensorMalfuntionText;
        }

        //make text
        StringBuilder builder = new StringBuilder();
        for (String s : text) {
            if(s != null) {
                if (builder.length() > 0) builder.append(textInBetweenMessages);
                builder.append(s);
                noErrors = false;
            }
        }

        //pass to method
        toggleErrorPanel(builder.toString(), !noErrors);
    }

    /**
     * Public method to build the main panel
     *
     * @return main panel
     */
    public JPanel buildMainPanel() {
        //build all buttons
        buildAllButtons();

        //add action listeners to all buttons
        addActionListenersToButtons();

        //set header title to current active button
        changeHeaderTitle();

        //set panel to right panel
        switchPanel(currentActiveButton.getName());

        //return panel
        return mainPanel;
    }

    /**
     * Build all buttons with buildSingleButtonMethod
     */
    @SuppressWarnings("DuplicatedCode")
    private void buildAllButtons() {

        // Sets button state of home button to active, because on opening programme you start at home
        setImageToButtonFromFilePath(homeButton, BtnState.ACTIVE);
        setImageToButtonFromFilePath(reportsButton, BtnState.DEFAULT);
        setImageToButtonFromFilePath(leaderboardButton, BtnState.DEFAULT);
        setImageToButtonFromFilePath(careButton, BtnState.DEFAULT);
        setImageToButtonFromFilePath(optionsButton, BtnState.DEFAULT);
        setImageToButtonFromFilePath(usersButton, BtnState.DEFAULT);
        setImageToButtonFromFilePath(helpButton, BtnState.DEFAULT);

        // Set layout and border to null for every button
        homeButton.setLayout(null);
        homeButton.setBorder(null);
        homeButton.setRolloverIcon(getButtonImageByState(homeButton, BtnState.HOVER));
        reportsButton.setLayout(null);
        reportsButton.setBorder(null);
        reportsButton.setRolloverIcon(getButtonImageByState(reportsButton, BtnState.HOVER));
        leaderboardButton.setLayout(null);
        leaderboardButton.setBorder(null);
        leaderboardButton.setRolloverIcon(getButtonImageByState(leaderboardButton, BtnState.HOVER));
        careButton.setLayout(null);
        careButton.setBorder(null);
        careButton.setRolloverIcon(getButtonImageByState(careButton, BtnState.HOVER));
        optionsButton.setLayout(null);
        optionsButton.setBorder(null);
        optionsButton.setRolloverIcon(getButtonImageByState(optionsButton, BtnState.HOVER));
        usersButton.setLayout(null);
        usersButton.setBorder(null);
        usersButton.setRolloverIcon(getButtonImageByState(usersButton, BtnState.HOVER));
        helpButton.setLayout(null);
        helpButton.setBorder(null);
        helpButton.setRolloverIcon(getButtonImageByState(helpButton, BtnState.HOVER));
    }

    /**
     * Add action listeners to all buttons
     */
    private void addActionListenersToButtons() {
        homeButton.addActionListener(e -> activateButton(homeButton));
        reportsButton.addActionListener(e -> activateButton(reportsButton));
        leaderboardButton.addActionListener(e -> activateButton(leaderboardButton));
        careButton.addActionListener(e -> activateButton(careButton));
        optionsButton.addActionListener(e -> activateButton(optionsButton));
        usersButton.addActionListener(e -> activateButton(usersButton));

        // easter egg for help button
        helpButton.addActionListener(e -> HelperFunctions.openWebpage("https://ec.europa.eu/international-partnerships/sustainable-development-goals_en", 1));
    }

    /**
     * Set passed button to active, and the button that was active to default
     *
     * @param button button that should be active
     */
    private void activateButton(@NotNull JButton button) {
        // Stop if pressed button is already active
        if (button.getName().equals(currentActiveButton.getName())) return;

        // Activate new button and set previous to default
        setImageToButtonFromFilePath(button, BtnState.ACTIVE);
        setImageToButtonFromFilePath(currentActiveButton, BtnState.DEFAULT);

        // Set centralized variables to right values
        currentActiveButton = button;

        // Change title with method according to button change
        changeHeaderTitle();

        // Change panel
        switchPanel(currentActiveButton.getName());
    }

    /**
     * Set image to parameterized button from filepath
     *
     * @param button button to which the image should be set
     * @param buttonState new state given button should be in
     */
    private void setImageToButtonFromFilePath(JButton button, BtnState buttonState) {
        // Append image to button
        button.setIcon(getButtonImageByState(button, buttonState));
        mainPanel.revalidate();
        mainPanel.repaint();
    }

    /**
     * Returns image icon for given button and it's state
     *
     * @param button button for which eh image should be retrieved
     * @param buttonState state the button is in
     * @return image icon for given button
     */
    private ImageIcon getButtonImageByState(JButton button, BtnState buttonState) {
        String filePath;
        String extension = "";

        // Set filepath corresponding to button state
        // 0 means default, 1 means hover and 2 means active
        if (buttonState == BtnState.DEFAULT) extension = "Default";
        else if (buttonState == BtnState.HOVER) extension = "Hover";
        else if (buttonState == BtnState.ACTIVE) extension = "Active";

        // Construct filepath from input
        filePath = iconsFilePath + button.getName() + extension + imgFileType;

        // Get image from filepath
        Image img = null;
        try {
            URL imgUrl = getClass().getClassLoader().getResource(filePath);
            if (imgUrl != null) img = ImageIO.read(imgUrl);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Scale image to right sizes
        return img != null ? new ImageIcon(img.getScaledInstance(iconWidth, iconHeight, Image.SCALE_SMOOTH)) : null;
    }

    /**
     * Method to change header title according to button that is currently set as active
     */
    private void changeHeaderTitle() {
        // Initialize variables
        String currBtnName = currentActiveButton.getName();

        String rawFirstPart;
        String rawSecondPart;
        String finalFirstPart;
        String finalPartialString;
        String finalFullString;
        String reportsToDutch = "Rapporten";
        String settingsToDutch = "Instellingen";
        String usersToDutch = "Gebruikers";

        // The titles of buttons are set in english, this UI is dutch.
        // Therefore we have to check if the 2 of the english names are selected
        // and we have to rewrite those to dutch
        if (currBtnName.equals(reportsBtnName)) {
            // If current active button is set to reports, set the partial title to reports in dutch
            finalPartialString = reportsToDutch;
        } else if (currBtnName.equals(optionsBtnName)) {
            // If current active button is set to settings, set partial title to settings in dutch
            finalPartialString = settingsToDutch;
        } else if (currBtnName.equals(usersBtnName)) {
            // If current active button is set to users, set partial title to users in dutch
            finalPartialString = usersToDutch;
        }else {
            // If both names are not equal to the reports or settings names,
            // we can set the title by doing some string operations on the current active button

            // Split the currently active button name to set first char to upper case
            rawFirstPart = currBtnName.substring(0, 1);
            rawSecondPart = currBtnName.substring(1);

            // Set first part or char to uppercase
            finalFirstPart = (rawFirstPart.toUpperCase());

            // Add strings together
            finalPartialString = (finalFirstPart + rawSecondPart);
        }

        // Build total title
        finalFullString = (defaultHeaderTitle + " - " + finalPartialString);

        // Set text to header with prefix
        headerLabelTitle.setText(finalFullString);
    }

    /**
     * Switch to new panel if button is pressed or other event happens
     * <p>
     * The parameter is name of panel, it should only give the name of the button pressed,
     * as "PanelForm" is automatically added in this method
     *
     * @param nameOfPanel name of panel to activate
     */
    public void switchPanel(String nameOfPanel) {
        // Determine view that should be requested
        View view;
        if (createdViews.containsKey(nameOfPanel))
            view = createdViews.get(nameOfPanel);
        else {
            if (nameOfPanel.equals(homeBtnName)) view = new HomePanelForm();
            else if (nameOfPanel.equals(reportsBtnName)) view = new ReportsPanelForm();
            else if (nameOfPanel.equals(leaderboardBtnName)) view = new LeaderboardPanelForm();
            else if (nameOfPanel.equals(careBtnName)) view = new CarePanelForm();
            else if (nameOfPanel.equals(optionsBtnName)) view = new OptionsPanelForm();
            else if (nameOfPanel.equals(usersBtnName)) view = new UserPanelForm();
            else throw new InvalidParameterException("Unexpected value: " + nameOfPanel);

            contentPanel.add(view.getPanel(), nameOfPanel);
            createdViews.put(nameOfPanel, view);
        }
        view.activate();

        // Set the obtained view as the content view
        ((CardLayout) contentPanel.getLayout()).show(contentPanel, nameOfPanel);
        mainPanel.revalidate();
        mainPanel.repaint();
    }
}