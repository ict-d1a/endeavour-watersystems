package com.endeavour.users;

import com.endeavour.Main;
import com.endeavour.View;
import org.jetbrains.annotations.Nls;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class UserPanelForm implements View {

    UsersTableModel usrModel;

    private JPanel mainUserPanel;
    private JButton newUser;
    private JTable usersTbl;
    private JPanel header;
    private JScrollPane content;

    private final UserModel userModel;

    public UserPanelForm() {
        this.userModel = Main.getUserModel();

        // Set listeners
        newUser.addActionListener(this::createNewUser);
        usersTbl.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                editUser(evt);
            }
        });
        usersTbl.setRowHeight(40);
    }

    private void editUser(java.awt.event.MouseEvent evt){
        int row = usersTbl.rowAtPoint(evt.getPoint());
        int col = usersTbl.columnAtPoint(evt.getPoint());
        if (row >= 0 && col == 5) {
            User user = EditUserModal.editExistingUser(mainUserPanel, usrModel.getUsers().get(row));
            if(user != null ) {
                if(user.isSetToDelete()) {
                    if (userModel.delete(user)) refreshUserList();
                } else if (userModel.updateExisting(user)) refreshUserList();
                else {
                    // TODO handle error
                }
            }
        }
    }

    private void createNewUser(ActionEvent actionEvent) {
        User user = EditUserModal.createNewUser(mainUserPanel);
        if(user != null ) {
            if(userModel.insertNew(user)) refreshUserList();
            else {
                // TODO handle error
            }
        }
    }

    @Override
    public JPanel getPanel() {
        return mainUserPanel;
    }

    @Override
    public void activate() {
        refreshUserList();
    }

    private void refreshUserList() {
        Map<String, User> users = userModel.retrieveUsers();
        if(users != null) {
            List<User> usersList = new ArrayList<>(users.values());
            usrModel = new UsersTableModel(usersList);
            usersTbl.setModel(usrModel);
        }
    }

    /**
     * Model for users table
     */
    static class UsersTableModel extends AbstractTableModel {

        private static final String first = "Voornaam";
        private static final String sur = "Achternaam";
        private static final String usr = "Gebruikersnaam";
        private static final String email = "Email";
        private static final String tel = "Telefoon";

        private final List<User> users;
        private final String[] columnNames = {first, sur, usr, email, tel, ""};

        private static final String editIconPath = "img/edit.png";
        private final Icon editIcon;

        UsersTableModel(List<User> users) {
            this.users = users;

            Image img = null;
            try {
                URL imgUrl = getClass().getClassLoader().getResource(editIconPath);
                if (imgUrl != null) img = ImageIO.read(imgUrl);
            } catch (IOException e) {
                e.printStackTrace();
            }

            // Scale image to right sizes
            editIcon = img != null ? new ImageIcon(img.getScaledInstance(20, 20, Image.SCALE_SMOOTH)) : null;
        }

        @Override
        public int getRowCount() {
            return users.size();
        }

        @Override
        public int getColumnCount() {
            return columnNames.length;
        }

        @Nls
        @Override
        public String getColumnName(int columnIndex) {
            return columnNames[columnIndex];
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            Object val = getValueAt(0, columnIndex);
            return val != null ? val.getClass() : String.class;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            User usr = users.get(rowIndex);
            return switch (columnIndex) {
                case 0 -> usr.getFirstName();
                case 1 -> usr.getLastName();
                case 2 -> usr.getUserName();
                case 3 -> usr.getEmail();
                case 4 -> usr.getPhone();
                case 5 -> editIcon;
                default -> throw new IllegalStateException("Unexpected value: " + columnIndex);
            };
        }

        public List<User> getUsers() {
            return users;
        }
    }
}
