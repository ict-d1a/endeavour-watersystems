package com.endeavour.users;

import com.endeavour.Main;
import com.endeavour.helpers.Encryptor;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class EditUserModal extends JDialog {

    private final UserModel userModel;
    private boolean pressedOk;
    private boolean setToDelete;

    private Icon showIcon;
    private Icon hideIcon;

    private JPanel contentPane;
    private JTextField firstNameField;
    private JTextField lastNameField;
    private JTextField userNameField;
    private JPasswordField passwordField;
    private JTextField emailField;
    private JTextField telField;
    private JLabel firstNameLbl;
    private JLabel lastNameLbl;
    private JLabel userNameLbl;
    private JLabel passwordLbl;
    private JButton showPassword;
    private JLabel emailLbl;
    private JLabel telLbl;
    private JPanel content;
    private JPanel footer;
    private JButton okBtn;
    private JButton cancelBtn;
    private JButton deleteBtn;

    private final static String createBtnName = "Gebruiker aanmaken";
    private final static String editBtnName = "Gebruiker bewerken";

    private EditUserModal(java.awt.Frame parent) {
        super(parent, true);
        this.userModel = Main.getUserModel();
        init();
    }

    private EditUserModal(java.awt.Dialog parent) {
        super(parent, true);
        this.userModel = Main.getUserModel();
        init();
    }

    private void init() {
        setContentPane(contentPane);
        getRootPane().setDefaultButton(okBtn);

        this.pressedOk = false;
        this.setToDelete = false;

        deleteBtn.addActionListener(e -> onDelete());
        cancelBtn.addActionListener(e -> onCancel());
        okBtn.addActionListener(e -> onOk());

        // toggle password visibility
        showIcon = new ImageIcon(getClass().getResource("/img/eye-regular.png"));
        hideIcon = new ImageIcon(getClass().getResource("/img/eye-slash.png"));
        showPassword.addActionListener(e -> {
            if(showPassword.getName().equals("show")){
                passwordField.setEchoChar((char) 0);
                showPassword.setIcon(hideIcon);
                showPassword.setName("hide");
            } else {
                passwordField.setEchoChar('*');
                showPassword.setIcon(showIcon);
                showPassword.setName("show");
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(e -> onCancel(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onDelete() {
        setToDelete = true;
        dispose();
    }

    private void onCancel() {
        dispose();
    }

    private void onOk() {
        pressedOk = true;
        dispose();
    }

    public static User createNewUser(Component parent){
        EditUserModal model = getDialog(parent);
        model.setGuiEditMode(false);
        model.setVisible(true);
        return model.pressedOk ? model.getUserFromForm() : null;
    }

    public static User editExistingUser(Component parent, User oldUser) {
        EditUserModal model = getDialog(parent);
        model.setGuiEditMode(true);
        model.addUserToForm(oldUser);
        model.setVisible(true);

        if(model.pressedOk || model.setToDelete) return model.writeNewUserToOld(oldUser, model.getUserFromForm());
        return null;
    }

    public void setGuiEditMode(boolean editMode){
        okBtn.setText(editMode ? editBtnName : createBtnName);
        deleteBtn.setVisible(editMode);
    }

    private User getUserFromForm() {
        // TODO add form validations

        User user = new User(
                userNameField.getText(),
                passwordField.getPassword().length > 0 ? Encryptor.getHash(passwordField.getPassword()) : null,
                firstNameField.getText(),
                lastNameField.getText(),
                emailField.getText(),
                telField.getText()
        );
        user.setToDelete(setToDelete);
        return user;
    }

    private void addUserToForm(User user) {
        userNameField.setText(user.getUserName());
        passwordField.setText("");
        firstNameField.setText(user.getFirstName());
        lastNameField.setText(user.getLastName());
        emailField.setText(user.getEmail());
        telField.setText(user.getPhone());
    }

    private User writeNewUserToOld(User oldUser, User newUser){
        oldUser.setUserName(newUser.getUserName());
        if(newUser.getHashedPassword() != null) oldUser.setHashedPassword(newUser.getHashedPassword());
        oldUser.setFirstName(newUser.getFirstName());
        oldUser.setLastName(newUser.getLastName());
        oldUser.setEmail(newUser.getEmail());
        oldUser.setPhone(newUser.getPhone());
        oldUser.setToDelete(newUser.isSetToDelete());
        return oldUser;
    }

    @NotNull
    private static EditUserModal getDialog(Component parent) {
        Window window = getWindow(parent);
        EditUserModal userModal;
        if (window instanceof Frame) userModal = new EditUserModal((Frame) window);
        else userModal = new EditUserModal((Dialog) window);

        userModal.setPreferredSize(new Dimension(550, 300));
        userModal.setLocationRelativeTo(null);
        userModal.pack();

        return userModal;
    }

    @NotNull
    @Contract("null -> new")
    private static Window getWindow(Component parent) {
        if (parent == null) {
            return new JFrame();
        } else if (parent instanceof Frame || parent instanceof Dialog) {
            return (Window) parent;
        } else {
            return getWindow(parent.getParent());
        }
    }
}