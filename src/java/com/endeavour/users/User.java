package com.endeavour.users;

import org.jetbrains.annotations.Nullable;

/**
 * Saves user- related data
 */
public class User {

    private boolean setToDelete;

    /**
     * Unique identifier
     * If null, the user is not yet present in the database
     */
    @Nullable
    private String id;

    private String userName;
    private String hashedPassword;
    private String name;
    private String surname;
    private String email;
    private String phone;

    /**
     * Create class for new user (without id)
     *
     * @param userName username
     * @param hashedPassword encrypted version of password
     * @param name first name
     * @param surname last name
     * @param email email address
     * @param phone phone number
     */
    User(String userName, String hashedPassword, String name, String surname, String email, String phone) {
        this(null, userName, hashedPassword, name, surname, email, phone);
    }

    /**
     * Create class for existing user (with id)
     *
     * @param id unique identifier user. If null, this is a new user
     * @param userName username
     * @param hashedPassword encrypted version of password
     * @param name first name
     * @param surname last name
     * @param email email address
     * @param phone phone number
     */
    User(@Nullable String id, String userName, String hashedPassword, String name, String surname, String email, String phone) {
        this.setToDelete = false;

        this.id = id;
        this.userName = userName;
        this.hashedPassword = hashedPassword;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.phone = phone;
    }

    public @Nullable String getId() {
        return id;
    }

    public void setId(@Nullable String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }

    public void setHashedPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }

    public String getFirstName() {
        return name;
    }

    public void setFirstName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return surname;
    }

    public void setLastName(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isSetToDelete() {
        return setToDelete;
    }

    public void setToDelete(boolean setToDelete) {
        this.setToDelete = setToDelete;
    }
}