package com.endeavour.users;

import com.endeavour.Model;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

public class UserModel {

    private final Connection con;
    private final Map<String, User> users;

    public UserModel() {
        con = Model.getConnection();
        users = retrieveUsers();
    }

    public boolean insertNew(@NotNull User user) {
        try {
            String sql = ("INSERT INTO users (`username`, `password`, `name`, `surname`, `email`, `phone`) VALUES (?, ?, ?, ?, ?, ?)");
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, user.getUserName());
            st.setString(2, user.getHashedPassword());
            st.setString(3, user.getFirstName());
            st.setString(4, user.getLastName());
            st.setString(5, user.getEmail());
            st.setString(6, user.getPhone());
            st.execute();
            return true;
        } catch (Exception e) {
            System.err.println("Gebruiker opslaan mislukt");
        }
        return false;
    }

    public boolean updateExisting(@NotNull User user) {
        try {
            String sql = ("UPDATE users SET `username` = ?, `password` = ?, `name` = ?, `surname` = ?, `email` = ?, `phone` = ? WHERE `id` = ?");
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, user.getUserName());
            st.setString(2, user.getHashedPassword());
            st.setString(3, user.getFirstName());
            st.setString(4, user.getLastName());
            st.setString(5, user.getEmail());
            st.setString(6, user.getPhone());
            st.setString(7, user.getId());
            return st.executeUpdate() != 0;
        } catch (Exception e) {
            System.err.println("Gebruiker updaten mislukt");
        }
        return false;
    }

    public boolean delete(@NotNull User user) {
        if(!user.isSetToDelete()) return false;
        try {
            String sql = ("DELETE FROM users WHERE id = ?");
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, user.getId());
            return st.executeUpdate() != 0;
        } catch (Exception e) {
            System.err.println("Gebruiker verwijderen mislukt");
        }
        return false;
    }

    /**
     * Retrieves the users from the database
     *
     * @return map with all users sorted by username
     */
    @Nullable
    public Map<String, User> retrieveUsers() {
        Map<String, User> data = new HashMap<>();
        try {
            Statement st = con.createStatement();
            String sql = ("SELECT id, username, password, name, surname, email, phone FROM users WHERE active = 1");
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                User thisUser = new User(
                        rs.getString("id"),
                        rs.getString("username"),
                        rs.getString("password"),
                        rs.getString("name"),
                        rs.getString("surname"),
                        rs.getString("email"),
                        rs.getString("phone"));
                data.put(thisUser.getUserName(), thisUser);
            }
        } catch (Exception e) {
            System.err.println("Gebruikers ophalen mislukt");
            return null;
        }
        return data;
    }

    @Nullable
    public Map<String, User> getUsers() {
        if(users.size() == 0) retrieveUsers();
        return users;
    }
}
