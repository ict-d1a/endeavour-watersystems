package com.endeavour.users;

import com.endeavour.Main;
import com.endeavour.helpers.Encryptor;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Map;

public class UserLogin extends JDialog {

    private final String defaultMessage;
    private final Font defaultFont;

    private final Icon showIcon;
    private final Icon hideIcon;

    private JPanel contentPane;
    private JButton buttonLogin;
    private JButton buttonCancel;
    private JTextField usernameField;
    private JPasswordField passwordField;
    private JLabel usrMessage;
    private JButton showPassword;

    private final UserModel userModel;

    public UserLogin() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonLogin);

        showIcon = new ImageIcon(getClass().getResource("/img/eye-regular.png"));
        hideIcon = new ImageIcon(getClass().getResource("/img/eye-slash.png"));

        buttonLogin.addActionListener(e -> onLogin());
        buttonCancel.addActionListener(e -> onCancel());
        userModel = Main.getUserModel();
        defaultMessage = usrMessage.getText();
        defaultFont = usrMessage.getFont();

        // toggle password visibility
        showPassword.addActionListener(e -> {
            if(showPassword.getName().equals("show")){
                passwordField.setEchoChar((char) 0);
                showPassword.setIcon(hideIcon);
                showPassword.setName("hide");
            } else {
                passwordField.setEchoChar('*');
                showPassword.setIcon(showIcon);
                showPassword.setName("show");
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(e -> onCancel(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    public static void loginUser() {
        UserLogin login = new UserLogin();
        login.setPreferredSize(new Dimension(550, 250));
        login.setLocationRelativeTo(null);
        login.pack();
        login.setModal(true);
        login.setVisible(true);
    }

    private void onLogin() {
        String username = usernameField.getText();
        char[] plainPassword = passwordField.getPassword();

        try {
            Map<String, User> users = userModel.getUsers();
            if (users.containsKey(username)) {
                boolean valid = Encryptor.validateHash(plainPassword, users.get(username).getHashedPassword());
                if(valid) dispose();
            }
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
        }

        // Error message
        usrMessage.setFont(new Font(defaultFont.getFontName(), Font.BOLD, defaultFont.getSize()));
        usrMessage.setForeground(Color.RED);
        usrMessage.setText("Wachtwoord niet juist. Probeer opnieuw");
    }

    private void onCancel() {
        System.exit(0);
    }

    public static void main(String[] args) {
        UserLogin dialog = new UserLogin();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
