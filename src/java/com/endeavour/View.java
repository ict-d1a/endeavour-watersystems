package com.endeavour;

import javax.swing.*;

/**
 * Interface used by all views within the system
 *
 * This forces every view that implements this interface to have all the methods listed below
 */
public interface View {

    /**
     * Build the view's panel
     *
     * @return view's panel
     */
    JPanel getPanel();

    /**
     * Fired every time the view is opened
     * Can be used to update data ect
     */
    void activate();
}
